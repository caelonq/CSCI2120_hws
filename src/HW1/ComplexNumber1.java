/**
 * Created by Caelon on 1/25/2017.
 */

import java.util.Scanner;
public class ComplexNumber1 {

    // The real part of the complex number.
    private final float a;

    // The imaginary part of the complex number.
    private final float b;

    /**
     * @param float real, imaginary
     */
    public ComplexNumber1(float a, float b) {
        this.a = a;
        this.b = b;
    }

    /**
     * @param ComplexNumber1 Obj
     * @return ComplexNumber1
     */
    public ComplexNumber1 add(ComplexNumber1 other) {
        float newA = this.a + Float.parseFloat(other.getA());
        float newB = this.b + Float.parseFloat(other.getB().replace("i", ""));
        return new ComplexNumber1(newA, newB);
    }

    /**
     * @param ComplexNumber1 Obj
     * @return ComplexNumber1
     */
    public ComplexNumber1 subtract(ComplexNumber1 other) {
        float newA = this.a - Float.parseFloat(other.getA());
        float newB = this.b - Float.parseFloat(other.getB().replace("i", ""));
        return new ComplexNumber1(newA, newB);
    }

    /**
     * @param ComplexNumber1 Obj
     * @return ComplexNumber1
     */
    public ComplexNumber1 multiply(ComplexNumber1 other) {
        float newA = this.a * Float.parseFloat(other.getA()) - this.b * Float.parseFloat(other.getB().replace("i", ""));
        float newB = this.b * Float.parseFloat(other.getA()) + this.a * Float.parseFloat(other.getB().replace("i", ""));
        return new ComplexNumber1(newA, newB);
    }

    /**
     * @param ComplexNumber1 Obj
     * @return ComplexNumber1
     */
    public ComplexNumber1 divide(ComplexNumber1 other) {
        float newA = ((this.a * Float.parseFloat(other.getA())) + (this.b * Float.parseFloat(other.getB().replace("i", "")))) /
                ((Float.parseFloat(other.getA()) * Float.parseFloat(other.getA())) + (Float.parseFloat(other.getB().replace("i", "")) *
                        Float.parseFloat(other.getB().replace("i", ""))));
        float newB = ((this.b * Float.parseFloat(other.getA())) - (this.a * Float.parseFloat(other.getB().replace("i", "")))) /
                (Float.parseFloat(other.getA()) * Float.parseFloat(other.getA()) + Float.parseFloat(other.getB().replace("i", "")) *
                        Float.parseFloat(other.getB().replace("i", "")));
        return new ComplexNumber1(newA, newB);
    }

    /**
     * @param ComplexNumber1 Obj
     * @return boolean
     */
    public boolean equals(ComplexNumber1 other) {
        boolean equal;
        equal = this.a == Float.parseFloat(other.getA()) && this.b == Float.parseFloat(other.getB().replace("i", ""));
        return equal;
    }

    /**
     * @param N/A
     * @return real float String
     */
    public String getA() {
        return Float.toString(this.a);
    }

    /**
     * @param N/A
     * @return imaginary float String
     */
    public String getB() {
        return Float.toString(this.b) + "i";
    }

    /**
     * @param N/A
     * @return ComplexNumber1 String
     */
    @Override
    public String toString() {
        return String.format("%s + %s", this.getA(), this.getB());
    }

}

class ComplexNumberCalculator {
    private static Scanner input = new Scanner(System.in);
    private static ComplexNumber1 firstNumber;

    public static void main(String[] args) {
        prompt();
    }

    public static void prompt() {
        boolean done = false;
        while (done == false) {
            System.out.println("Enter Complex Number\nExample:\n\"a+bi\""+
            "\nEnter X to exit");
            String ans = input.nextLine();
            if (ans.matches("\\d*\\+\\d*i")) {

                String[] numbers = ans.split("\\+");
                for (int i = 0; i < numbers.length; ++i) {
                    firstNumber = new ComplexNumber1(Float.parseFloat(numbers[0]),
                            Float.parseFloat(numbers[1].replace("i", "")));
                    complexCalc();
                    done = true;
                }
            }
            else if(ans.equalsIgnoreCase("x")){
                done = true;
            }
            else {
                System.out.println("ERROR");
            }

        }
    }

    public static void complexCalc() {
        String ans = "";
        ComplexNumber1 newNumber = null;
        boolean done = false;
        while(done == false){
            System.out.println("Orignial Complex Number: "+firstNumber.toString());
            System.out.println("Enter:\n+ to add\n- to subtract" +
                "\n* to multiply\n/ to divide\nX to exit");
        String operator = input.nextLine();

        if (operator.equals("+")) {
            System.out.println("Enter Complex Number\nExample:\n\"a+bi\"");
            ans = input.nextLine();
            String[] numbers = ans.split("\\+");
            for (int i = 0; i < numbers.length; ++i) {
                 newNumber = new ComplexNumber1(Float.parseFloat(numbers[0]),
                        Float.parseFloat(numbers[1].replace("i", "")));}
                System.out.println("("+firstNumber.toString() + ") + (" + newNumber.toString()+") " +
                        "....");
                ComplexNumber1 sum = firstNumber.add(newNumber);
                System.out.println("ANSWER: "+sum.toString());
                done = true;

        } else if (operator.equals("-")) {
            System.out.println("Enter Complex Number\nExample:\n\"a+bi\"");
            ans = input.nextLine();
            String[] numbers = ans.split("\\+");
            for (int i = 0; i < numbers.length; ++i) {
                newNumber = new ComplexNumber1(Float.parseFloat(numbers[0]),
                        Float.parseFloat(numbers[1].replace("i", "")));}
                System.out.println("("+firstNumber.toString() + ") - (" + newNumber.toString()+") " +
                        "....");
                ComplexNumber1 difference = firstNumber.subtract(newNumber);
                System.out.println("ANSWER: "+difference.toString());
                done = true;

        } else if (operator.equals("*")) {
            System.out.println("Enter Complex Number\nExample:\n\"a+bi\"");
            ans = input.nextLine();
            String[] numbers = ans.split("\\+");
            for (int i = 0; i < numbers.length; ++i) {
                newNumber = new ComplexNumber1(Float.parseFloat(numbers[0]),
                        Float.parseFloat(numbers[1].replace("i", "")));}
                System.out.println("("+firstNumber.toString() + ") * (" + newNumber.toString()+") " +
                        "....");
                ComplexNumber1 product = firstNumber.multiply(newNumber);
                System.out.println("ANSWER: "+product.toString());
                done = true;

        } else if (operator.equals("/")) {
            System.out.println("Enter Complex Number\nExample:\n\"a+bi\"");
            ans = input.nextLine();
            String[] numbers = ans.split("\\+");
            for(int i = 0; i < numbers.length; ++i) {
                newNumber = new ComplexNumber1(Float.parseFloat(numbers[0]),
                        Float.parseFloat(numbers[1].replace("i", ".0")));}
                System.out.println("("+firstNumber.toString() + ") / (" + newNumber.toString()+") "+
                        "....");
                ComplexNumber1 quotient = firstNumber.divide(newNumber);
                System.out.println("ANSWER: "+quotient.toString());
                done = true;

        } else if (operator.equalsIgnoreCase("x")) {
            done = true;
        }
        }
    }
}