
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream; 
import java.io.FileOutputStream; 
import java.io.IOException;
import java.io.EOFException;
import java.util.Scanner;




public class GroceryTracker implements java.io.Serializable
{
	private ObjectInputStream reader = null;
	private ObjectOutputStream writer = null;
	private GroceryList list = new GroceryList("My Groceries");
	private String file = "Grocery.ser";
	private Scanner input = new Scanner(System.in);
	private int key = 0;

	GroceryTracker()
	{
		
	}
	
	public void menu()
	{	
		while(true){ 
			
			System.out.printf("Welcome to GroceryTracker!\nCurrent File: %s\n Please select ", file);
			System.out.printf("an option:\n\t1) View List\n\t2) Add An ");
			System.out.printf("Item\n\t3) Remove An Item\n\t4) Save\n\t5) Load\n\t6) Quit\nEnter: ");
			String in = input.nextLine();
			System.out.println("\n");

				if(in.matches("[\\d]"))
					key = Integer.parseInt(in);

			switch(key)
			{
				case 1:
					printList();
					break;
				case 2:
					addItem();
					break;
				case 3:
					remove();
					break;
				case 4:
					save();
					break;
				case 5:
					loadList();
					break;
				case 6:
					list.clear();
					System.exit(0);
					break;
				default:
					System.out.println("Invalid Key ");
					break;
			}
	}


	}

	public void remove()
	{	
	
		System.out.printf("\n\nOk. . . to remove item enter Item Number. \n%s\n\nItem Number: ", list.toString());
		String itemNumber = input.nextLine();

		System.out.printf("Removed Item\t%s\n\n\n\n", list.removeItemAtIndex((Integer.parseInt(itemNumber)-1)));

	}

	public void printList()
	{
		System.out.println("\n"+list.toString()+"\n\n\n");
	}


 	public void loadList()
 	{	
 		list.clear();
 		try
		{
			reader = new ObjectInputStream(new FileInputStream(file));

			while(true)
			{	
				list = (GroceryList) reader.readObject();
			}
		}
		catch(NullPointerException e)
		{
			System.out.println("InputStream Not Previously Opened.");
			System.exit(1); 
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Object Type Invalid ");
			System.exit(1);
		}
		catch(EOFException e)
		{	
			System.out.println(list.toString());
			System.out.println("Last Of Items\n\n\n");
		}
		catch(IOException e)
		{
			System.out.println("Error opening file");
		}
		try
		{
			reader.close();
		}
		catch(IOException e)
		{
			System.out.println("Error Closing File");
			System.exit(1);
		}

 	}


 	public void addItem()
 	{	
 		
 			
 			System.out.println("\nEnter Item: ");
 			String item = input.nextLine();
 			System.out.println("\n\n\n");
 			list.addItem(item);
 	}

 	public void save()
 	{	


		 	if(list != null)
		 	{
		 		try
				{
					writer = new ObjectOutputStream(new FileOutputStream(file));
					writer.writeObject(list);
				}
				catch(IOException e)
				{
					System.out.println();
				}
				catch(NullPointerException e)
				{
					System.out.println("OutputStream Not Previously Opened.");
					System.exit(1); 
				}
				try
				{
					writer.close();
					System.out.println("Succesively Saved\n\n");
				}
				catch(IOException e)
				{
					System.out.println("Error Closing File");
					System.exit(1);
				}


			}
		
 	}


}