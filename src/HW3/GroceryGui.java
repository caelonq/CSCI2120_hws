import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.Box;
import java.awt.Container;
import javax.swing.JTextField;


public class GroceryGui extends JFrame implements ActionListener, java.io.Serializable
{

	private JButton[] buttons = new JButton[5];
	private JTextArea notepad;
	private Container buttonContainer;
	private Container inputContainer;
	private Container grouper;
	private Box box;
	private JTextField retrieveInput;
	private JTextField addInput;
	private GridLayout buttonGrid;
	private GridLayout inputGrid;
	private FlowLayout flow;
	private GroceryTrackerGui groceries;
	private String itemNumber = "";
	private String itemAdded = "";


	GroceryGui()
	{	
		
		super("Grocery List");
		groceries = new GroceryTrackerGui();
		box = Box.createVerticalBox();
		buttonContainer = new Container();
		inputContainer = new Container();
		grouper = new Container();
		notepad = new JTextArea(groceries.getList(), 15, 15);
		retrieveInput = new JTextField("Enter Number Here To Remove Item ", 30);
		addInput = new JTextField("Add Item Here ", 30);
		String[] options = {"Add Item", "Remove Item", "Save List", "Load List", "View List"};

		retrieveInput.setEditable(true);
		addInput.setEditable(true);
		notepad.setEditable(false);

		buttonGrid = new GridLayout(5, 1, 0, 4);
		inputGrid = new GridLayout(2, 1, 0, 11);
		flow = new FlowLayout(FlowLayout.CENTER, 8, 0);

		inputContainer.setLayout(inputGrid);
		addInput.addActionListener(this);
		retrieveInput.addActionListener(this);
		inputContainer.add(addInput);
		inputContainer.add(retrieveInput);

		buttonContainer.setSize(20, 40);
		buttonContainer.setLayout(buttonGrid);

		grouper.setLayout(flow);


		for(int i = 0; i < options.length; ++i)
		{	
			this.buttons[i] = new JButton(options[i]);
			buttons[i].addActionListener(this);
			buttonContainer.add(buttons[i]);
		}

		box.add(buttonContainer);	
		box.add(Box.createVerticalGlue());
		box.add( new JScrollPane(notepad));
		box.add(Box.createHorizontalGlue());

		grouper.add(box);
		grouper.add(inputContainer);

		setLayout(new FlowLayout());
		add(grouper);
	}//End of Constructor

	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == buttons[0]) 
		{
			groceries.addItem(itemAdded);
			notepad.setText(groceries.getList()); // Block For Adding Items After Entered
		}
		else if(event.getSource() == addInput)   
		{	
			itemAdded = event.getActionCommand(); // Block For Entering In Items
			notepad.setText(itemAdded);
		
		}

		else if (event.getSource() == buttons[1]) 
		{	
			if (itemNumber.matches("[\\d]"))
				groceries.remove(itemNumber);
			notepad.setText(groceries.getList());	// Block For Removing Items After Item Number Is Entered
		}	
		else if(event.getSource() == retrieveInput)
		{
			itemNumber = event.getActionCommand();	// Block For Entering In Item Numbers
		}

		else if (event.getSource() == buttons[2]) 
		{
			groceries.save();						// Block For Saving Grocery List
		}
		else if (event.getSource() == buttons[3]) 
		{
			groceries.loadList();
			notepad.setText(groceries.getList());	// Block For Loading List From File
		}

		else if (event.getSource() == buttons[4]) 
		{
			notepad.setText(groceries.getList());	// Block For Viewing List
		}
	}
}