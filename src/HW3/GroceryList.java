import java.util.ArrayList;

/**
 * A class that models a list of grocery items.
 *
 * Hint: Use ArrayList to do all the heavy lifting for storing your items.
 */
public class GroceryList implements java.io.Serializable{
    private static String name = "List";
    private ArrayList<String> list;
    /**
     * Constructs a new GroceryList object that is initially empty.
     */
    public GroceryList(String listname){
        this.name = listname;
        list = new ArrayList<String>();
    }

    public GroceryList(){
        this(name);
        list = new ArrayList<String>();
    }

    /**
     * Returns the number of items currently in the grocery list.
     *
     * @return The number of items in the list.
     */
    public int getItemCount(){
        return list.size();
    }

    /**
     * If the given item String is not in the list yet (ignoring capitalization
     * and leading/trailing whitespace), appends the item to the end
     * of the list. Otherwise, does nothing. A GroceryList should be able
     * to hold as many unique item names as the user desires. If the item
     * contains no actual text other than whitespace, this should not be added.
     *
     * @param item The item to add.
     */
    public void addItem(final String item){
        
        list.add(item);
       this.formatList();
    }

    /**
     * Removes the item at the specified index. If the index specified is
     * >= this.getItemCount(), an IllegalArgumentException should be thrown.
     * After removal of an item, the index of all items past the specified index
     * should be decremented. E.g.:
     *
     * Before:
     * 0 => Eggs
     * 1 => Milk
     * 2 => Spinach
     *
     * list.removeItemAtIndex(1);
     *
     * After:
     * 0 => Eggs
     * 1 => Spinach
     *
     * @param index The index (zero-based) to remove.
     */
    public String removeItemAtIndex(final int index){

            String removedItem = "";

            if(list.size() <= (index))
                throw new IllegalArgumentException("Insuffucient number of items at specified index");
            else
            {   
                removedItem = this.getItemAtIndex(index);
                list.remove(index); 
            }

            return removedItem;
        
        //this.formatList();
    }

    /**
     * Returns the item String at the specified index. If the index specified
     * is >= this.getItemCount(), an IllegalArgumentException should be thrown. The
     * String returned should be given in "canonical" form", that is, with no leading/trailing
     * whitespace and the first letter of each individual word capitalized, regardless of
     * how the item was added initially. E.g.:
     *
     * "eggs" => "Eggs"
     * "toilet paper" => "Toilet Paper"
     * "MILK" => "Milk"
     * "  coffee " => "Coffee"
     *
     * @param index The index (zero-based) to fetch.
     * @return The grocery item text at the given index, in the canonical form specified above.
     */
    public String getItemAtIndex(final int index){
            
        String item = null;
          
            if(list.size() <= index)
                throw new IllegalArgumentException("Insuffucient number of items at specified index");
            else{
               item = list.get(index); 
            }

            return item;
    }

    /**
     * @{inheritDoc}
     *
     * Returns a String representation of this list. Should use the StringBuilder class to build
     * up the result. A representation of a GroceryList is a human-readable series of lines with
     * a line number (1-based), followed by a period and space (". "), followed by the item. There
     * should be no trailing newline. If the list is empty, the words "(Empty List)" should be returned.
     *
     * E.g. for GroceryList {0 => "Eggs", 1 => "Bacon", 2 => "Bread"}, it should return:
     *
     * "1. Eggs
     * 2. Bacon
     * 3. Bread"
     */
    @Override
    public String toString(){

       StringBuilder strings = new StringBuilder();
       int itemNumber = 0;
       if(this.getItemCount() > 0)
       {
            this.formatList();
            Object[] itemArray = list.toArray();
            for(Object ob : itemArray)
            {
                ++itemNumber;
                if(itemNumber < itemArray.length)
                    strings.append(itemNumber+". "+ob+"\n");
                else
                    strings.append(itemNumber+". "+ob);
            }
        }
        else
            strings.append("(Empty List)");
        
        
       

        return strings.toString();
    }

    /**
    * Formats List In Correct Format
    */
    public void formatList()
    {
        Object[] items = list.toArray(); // Object Array To Hold Items From The Array List;
        String[] itemArray = new String[items.length]; // String Array to hold Items Converted from the Object Array;
        char[] characters; // Char Array To Hold The String's Characters from the String Array;
        list.clear(); // Clears The List For Reassignment;
        StringBuilder string = new StringBuilder(); // String Builder For Adding Formatted Characters;
        int count = 0; // Counts nummber of Items;
        String lastSpace = "LastSpace"; // Assigns The Last Space In The String;
        boolean indexed = false; // Becomes True If The First Character Is Capitalized & Added To The StringBuilder;
        boolean space = false; // Becomes True If A Space Is Encountered;
        String character = "Character"; // Holds a Single Character;

        for(Object item : items)
        {
            itemArray[count] = (String) item;
            ++count;
        }

        for (String k : itemArray) // Iterates Through Item Array;
        {  
            characters = k.toCharArray(); // Retrieves The Characters Of Each Item;
            indexed = false;
                    for(char c : characters) // Iterates Through The List Of Characters Retrieved From an Item;
                    { 
                        character = String.format("%c", c); // Converts Character To A String For String Manipulation;

                        // Checks The Characters Using Match Cases;
                        if(character.matches("[a-z]|[A-Z]") && indexed == false) 
                        {
                            if(lastSpace.matches("[\\s]+"))
                            {
                                string.append(lastSpace);
                            }
                            
                            lastSpace = "LastSpace";
                            indexed = true;
                            string.append(character.toUpperCase());
                            space = false;
                        }
                        else if(character.matches("[\\s]+") && indexed == false)
                        {
                            indexed = false;
                        }
                        else if(character.matches("[a-z]|[A-Z]") && indexed == true)
                        {
                            indexed = true;
                            string.append(character.toLowerCase());
                        }
                        else if(character.matches("[\\s]+") && indexed == true && space == false)
                        {   
                            lastSpace = character;
                            indexed = false;
                            space = true;
                        }
                     
                    }
                    list.add(string.toString()); // Adds The New Representation Of The Item To The List;
                    string.delete(0, characters.length);  // Clears The StringBuilder;
        }
    }

    public void clear()
    {
        list.clear();
    }

}
