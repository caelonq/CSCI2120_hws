
import javax.swing.JFrame;
public class GroceryTrackerApp extends JFrame implements java.io.Serializable
{
	public static void main(String[] args)
	{
		GroceryGui app = new GroceryGui();

		app.setVisible(true);
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.setSize(700, 700);
	}
}