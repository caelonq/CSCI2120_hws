
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream; 
import java.io.FileOutputStream; 
import java.io.IOException;
import java.io.EOFException;
import javax.swing.JOptionPane;



public class GroceryTrackerGui implements java.io.Serializable
{
	private ObjectInputStream reader = null;
	private ObjectOutputStream writer = null;
	private GroceryList list = new GroceryList("My Groceries");
	private String file = "Grocery.ser";

	GroceryTrackerGui()
	{
		
	}
	

	public void remove(String itemNumber)
	{	
	
		String removedItem = String.format("Removed Item:  %s", list.removeItemAtIndex((Integer.parseInt(itemNumber)-1)));
		JOptionPane.showMessageDialog(null, removedItem, "Removing Item", JOptionPane.INFORMATION_MESSAGE);

	}

	public String getList()
	{
		return list.toString();
	}


 	public void loadList()
 	{	
 		list.clear();
 		try
		{
			reader = new ObjectInputStream(new FileInputStream(file));

			while(true)
			{	
				list = (GroceryList) reader.readObject();
			}
		}
		catch(NullPointerException e)
		{
			JOptionPane.showMessageDialog(null, "InputStream Not Previously Opened.", "Loading List", JOptionPane.ERROR_MESSAGE);
			System.exit(1); 
		}
		catch(ClassNotFoundException e)
		{
			JOptionPane.showMessageDialog(null, "Object Type Invalid ", "Loading List", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		catch(EOFException e)
		{	
			JOptionPane.showMessageDialog(null, "Loading List. . . \n", "Loading List", JOptionPane.INFORMATION_MESSAGE);
		}
		catch(IOException e)
		{
			JOptionPane.showMessageDialog(null, "Error opening file", "Loading List", JOptionPane.ERROR_MESSAGE);
		}
		try
		{
			reader.close();
		}
		catch(IOException e)
		{
			JOptionPane.showMessageDialog(null, "Error Closing File", "Loading List", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

 	}


 	public void addItem(String item)
 	{	
 			if(!(item.equals("")))
 				list.addItem(item);
 	}

 	public void save()
 	{	


		 	if(list != null)
		 	{
		 		try
				{
					writer = new ObjectOutputStream(new FileOutputStream(file));
					writer.writeObject(list);
				}
				catch(IOException e)
				{
					JOptionPane.showMessageDialog(null, "Error opening file", "Saving List", JOptionPane.ERROR_MESSAGE);
				}
				catch(NullPointerException e)
				{
					JOptionPane.showMessageDialog(null, "OutputStream Not Previously Opened.", "Saving List", JOptionPane.ERROR_MESSAGE);
					System.exit(1); 
				}
				try
				{
					writer.close();
					JOptionPane.showMessageDialog(null, "Succesively Saved\n\n", "Saving List", JOptionPane.INFORMATION_MESSAGE);
				}
				catch(IOException e)
				{
					JOptionPane.showMessageDialog(null, "Error Closing File", "Saving List", JOptionPane.ERROR_MESSAGE);
					System.exit(1);
				}


			}
		
 	}


}