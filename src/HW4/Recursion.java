/**
 * @author 	Caelon Queen
 * @version 21 February 2017
 */

public class Recursion
{
	/**
	 * @params int
	 * @return boolean value determining if the integer is even.
	 */
	public static boolean isEven(int n)
	{	
		boolean bool = false;


			 if(n%2 == 0)
			 	bool = true;
		
		return bool;
	}

	/**
	 * @params int
	 * @return boolean value determining if the integer is odd.
	 */
	public static boolean isOdd(int n)
	{	
		boolean bool = false;

		if(isEven(n) == false)
			bool = true;	
		
		return bool;
	}



	/*public static boolean isEven(int n)
	{														*****************************************
		if(isOdd(n) == false)
			return true;									*										*
		else												*	<~~~ StackOverFlow `laughing emoji`	*
			return false;									*										*
	}
															*****************************************

	public static boolean isOdd(int n)
	{
		if(isEven(n) == false)
			return true;
		else
			return false;	
	}*/

	/**
	 * @params int, int
	 * @return int sum of a number occurring b times.
	 */
	public static int multiply(int a, int b)
	{
		int aggregate = b;
		int product = a;

		if(aggregate >= 1)
		{
			--aggregate;
			product += multiply(a, aggregate);
		}
		else
			product = 0;

		return product;
	}
}