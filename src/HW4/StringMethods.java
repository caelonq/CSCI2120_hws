/**
 * @author 	Caelon Queen
 * @version 21 February 2017
 */


public class StringMethods {

    /**
     * No reason to instantiate this class, so the constructor is private.
     */
    private StringMethods() {

    }

    /*
     * TODO: Fill in the areEqual and isPalindrome methods below with recursive implementations.
     *
     * These methods are recursive, so they either solve the problem right away (the base case),
     * or reduce the input to a simpler problem before calling themselves again (the recursive case).
     */

    ///////////////////////////////////////////////////////////
    // PART ONE
    ///////////////////////////////////////////////////////////

    /**
     * Returns whether two NCStrings are equal. The comparison respects case (so 'test' != 'Test').
     *
     * @return Whether two NCStrings are equal.
     */
    public static boolean areEqual(final NCString a, final NCString b) {
        boolean equal = false;


        if (a.hashCode() == b.hashCode()) {
            equal = true;
        } else {
            equal = false;
        }


        return equal;
    }

    ///////////////////////////////////////////////////////////
    // PART TWO
    ///////////////////////////////////////////////////////////  

    /**
     * Returns whether two NCStrings are palindromes. A String is a palindrome if it reads the
     * same forwards and backwards, respecting case and whitespace. E.g.:
     * <p>
     * "racecar"
     * "level"
     * "a"
     * "aa"
     * "mom"
     * "radar radar"
     * ""           <-- the empty string.
     * <p>
     * The following would not be considered palindromes:
     * <p>
     * "example"
     * "top spot"   <-- because of the space
     * "Mom"        <-- because of the capitalization
     *
     * @param s The NCString to examine. Can be assumed to be non-null.
     * @return Whether the argument is a palindrome, as defined above.
     */
    public static boolean isPalindrome(final NCString s) {
        boolean isPalindrome = false;

        if (s.getLength() == 1 || s.getLength() == 0) {
            isPalindrome = true;
        } else if ((s.getFirstChar() == s.getLastChar()) && (s.getLength() > 1)) {
            isPalindrome = isPalindrome(s.getMiddleChars());

        }


        return isPalindrome;
    }

    /**
     * @params A non-null NCString.
     * @return int The number of occurrences a character appears in a NCString.
     */
    public static int characterCount(NCString s, char c) {
        int tally = 0;
        int count = 0;


        if(s.getLength() > 0) {
            if (s.getLength() == 1 && s.getLastChar() == c) {
            tally += 1;
            }
            if (s.getFirstChar() == c) {
            tally += 1;
            }
            if (s.getLastChar() == c) {
            tally += 1;
            }
        }
        if(s.getLength() > 1)
            count+=characterCount(s.getMiddleChars(), c);

        return tally + count;
    }
}


