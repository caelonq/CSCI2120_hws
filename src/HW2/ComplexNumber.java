/**
 * Created by Caelon on 1/31/2017.
 */
    public class ComplexNumber
{
        /**
         * instance variables a and b respresenting
         * the real and imaginary part of the complex number
         */
        private float a;
        private float b;

        /**
         * Construction Method
         * @param a the real part of the complex number
         * @param b the imaginary part of the complex number
         */
        public ComplexNumber(float a, float b){
            this.a = a;
            this.b = b;
        }

        /**
         * Method that gets real part of the complex number
         * @return the real part of the complex number
         */
        public float getA(){
            return this.a;
        }

        /**
         * Method that gets imaginary part of the complex number
         * @return the imaginary part of the complex number
         */
        public float getB(){
            return this.b;
        }

        /**
         * Method that adds a number with the complex number
         * @param otherNumber the number to be added to this number
         */
        public ComplexNumber add(ComplexNumber otherNumber){
            ComplexNumber sum;
            float newA = this.a + otherNumber.getA();
            float newB = this.b + otherNumber.getB();
            sum = new ComplexNumber(newA, newB);
            return sum;
        }

        /**
         * Method that subtracts a number from the complex number
         * @param otherNumber the number to be subtracted from this number
         */

        public ComplexNumber subtract(ComplexNumber otherNumber){
            ComplexNumber difference;
            float newA = this.a - otherNumber.getA();
            float newB = this.b - otherNumber.getB();
            difference = new ComplexNumber(newA, newB);
            return difference;
        }

        /**
         * Method that multiplies a number with the complex number
         * @param otherNumber the number to be multiplied to this number
         */

        public ComplexNumber multiply(ComplexNumber otherNumber){
            ComplexNumber product;
            float newA = this.a * otherNumber.getA() - this.b * otherNumber.getB();
            float newB = this.b * otherNumber.getA() + this.a * otherNumber.getB();
            product = new ComplexNumber(newA, newB);
            return product;
        }

        /**
         * Method that divides a number with the complex number
         * @param otherNumber the number to be divided from this number
         */

        public ComplexNumber divide(ComplexNumber otherNumber){
            ComplexNumber quotient;
            float newA = (this.a * otherNumber.getA() + this.b * otherNumber.getB()) /
                    (otherNumber.getA() * otherNumber.getA() + otherNumber.getB() * otherNumber.getB());
            float newB = (this.b * otherNumber.getA() - this.a * otherNumber.getB()) /
                    (otherNumber.getA() * otherNumber.getA() + otherNumber.getB() * otherNumber.getB());
            quotient = new ComplexNumber(newA, newB);
            return quotient;
        }
        
        @Override
        public String toString(){
            return String.format("%f + %fi", getA(), getB());
        }

    /**
     * @param ComplexNumber Obj
     * @return boolean
     */
    public boolean equals(ComplexNumber other) {
        boolean equal;
        equal = this.a == other.getA() && this.b == other.getB();
        return equal;
    }

    }

