import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.Before;

/**
 * ComplexNumber Tester
 */

public class ComplexNumberTest{
	/**
	 * Floating point precision for the test
	 */

//	private static final float EPSILON = 1e-3;

	/**
	 * Test fixtures
	 */

	private ComplexNumber num1;
	private ComplexNumber num2;
	private ComplexNumber num3;
	private ComplexNumber num4;

	/**
	 * Method that sets up the test fixtures
	 */

	@Before

	public void setUp(){
		num1 = new ComplexNumber(2,4);
		num2 = new ComplexNumber(9,5);
		num3 = new ComplexNumber(3,9);
		num4 = new ComplexNumber(3,7);
	}

	/**
	 * Method that tests addition of ComplexNumbers
	 */

	@Test

	public void testAdd(){
		ComplexNumber num12 = num1.add(num2);
		assertNotSame(new ComplexNumber(99, 99),num12);
		assertEquals(9,num12.getB(), .003);
		ComplexNumber num13 = num1.add(num3);
		assertEquals(5,num13.getA(), .003);
		assertEquals(13,num13.getB(), .003);

		ComplexNumber num14 = num1.add(num4);
		assertEquals(5,num14.getA(), .003);
		assertEquals(11,num14.getB(), .003);
	}

	/**
	 * Method that tests subtraction of ComplexNumbers
	 */

	@Test

	public void testSubtract(){
		ComplexNumber num21 = num2.subtract(num1);
		assertEquals(7,num21.getA(), .003);
		assertEquals(1,num21.getB(), .003);

		ComplexNumber num23 = num2.subtract(num3);
		assertEquals(6,num23.getA(), .003);
		assertEquals(-4,num23.getB(), .003);

		ComplexNumber num24 = num2.subtract(num4);
		assertEquals(6,num24.getA(), .003);
		assertEquals(-2,num24.getB(), .003);
	}

	/**
	 * Method that tests multiplication of ComplexNumbers
	 */

	@Test

	public void testMultiply(){
		ComplexNumber num31 = num3.multiply(num1);
		assertEquals(-30,num31.getA(), .003);
		assertEquals(30,num31.getB(), .003);

		ComplexNumber num32 = num3.multiply(num2);
		assertEquals(-18,num32.getA(), .003);
		assertEquals(96,num32.getB(), .003);

		ComplexNumber num34 = num3.multiply(num4);
		assertEquals(-54,num34.getA(), .003);
		assertEquals(48,num34.getB(), .003);
	}

	/**
	 * Method that tests division of ComplexNumbers
	 */

	@Test

	public void testDivide(){
		ComplexNumber num41 = num4.divide(num1);
		assertEquals(1.7,num41.getA(), .003);
		assertEquals(.1,num41.getB(), .003);

		ComplexNumber num42 = num4.divide(num2);
		assertEquals(.5849057,num42.getA(), .003);
		assertEquals(.4528302,num42.getB(), .003);

		ComplexNumber num43 = num4.divide(num3);
		assertEquals(.8,num43.getA(), .003);
		assertEquals(-.06666667,num43.getB(), .003);
	}

	@Test
	public void addingZeroShouldNotChangeValue(){
		ComplexNumber complexZero = new ComplexNumber(0,0);
		ComplexNumber someNumber = new ComplexNumber(98, 99);
		assertTrue(someNumber.add(complexZero).equals(someNumber));
	}
}
